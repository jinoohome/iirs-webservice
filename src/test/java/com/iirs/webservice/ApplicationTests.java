package com.iirs.webservice;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.iirs.webservice.exper.dao.ExperDao;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {
	 @Autowired
	    private ExperDao experDao;

	@Test
	public void getExperListTest() {
		 
		//when
        List<Map<String, Object>> experList = experDao.getExperList("");
        
        //then
        Map<String, Object> exper = experList.get(0);
        assertThat(exper.get("EXPER_NM"));
	}
}

