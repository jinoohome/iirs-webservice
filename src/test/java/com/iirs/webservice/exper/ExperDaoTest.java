package com.iirs.webservice.exper;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.iirs.webservice.exper.dao.ExperDao;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExperDaoTest {
	
	@Autowired
	ExperDao experDao;
	
	@After
	public void cleanup() {
		System.out.println("cleanup");
	}
	
	@Test
	public void insertExerInfo() {
	
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("experNm", "데이터 확인 ");
		boolean result = experDao.insertExerInfo(param);
	}
	

}
