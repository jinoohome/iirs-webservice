package com.iirs.webservice.file.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.iirs.webservice.file.dao.FileDao;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class FileService {
	private FileDao fileDao;


	public List<Map<String, Object>> getFileList(String experId) {
		return fileDao.getFileList(experId);
	}


	public int getFileListMaxSeq() {
		return fileDao.getFileListMaxSeq();
	}


	public int getFileListMaxContentSeq(String experId) {
		return fileDao.getFileListMaxContentSeq(experId);
	}


	public boolean insertFileList(Map<String, Object> param) {
		int seq = fileDao.getFileListMaxSeq();
		param.put("seq", seq);
		return fileDao.insertFileList(param);
	}


	public boolean deleteFile(String seq) {
		return fileDao.deleteFile(seq);
	}



}
