package com.iirs.webservice.file.web;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.activation.MimetypesFileTypeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.iirs.webservice.file.service.FileService;

import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
public class FileController {
	
	 private FileService fileService;
	 private static final Logger logger = LogManager.getLogger();
	 private static final String fileDir = System.getProperty("user.dir")+"\\src\\main\\resources\\static\\image\\";
		
	 
	 @GetMapping("/file/1")
	 public String getExper(@RequestParam(required=false) String experId, Model model) {
		 List<Map<String,Object>> fileList = new ArrayList<Map<String,Object>>();
		 try{
			 fileList = fileService.getFileList(experId);
		 }catch(Exception e){
			 logger.info(e);
		 }
		 model.addAttribute("fileList",fileList);
		 return "exper/experhome";
	 }
	 
	 
	 @PostMapping("/file/2")
	 @ResponseBody
	 public boolean insertFile(@RequestParam("fileList") MultipartFile[] fileList, @RequestParam Map<String,Object> param)throws IOException  {
		 Calendar cal = Calendar.getInstance( );
		 String year = Integer.toString( cal.get(Calendar.YEAR) );
		 int monthCnt = cal.get(Calendar.MONTH) + 1;
		 String month =  ( monthCnt < 10 ? "0" : "" )  + Integer.toString( monthCnt );
		 
		 //파일저장하기
		 String contentCode = String.valueOf(param.get("content_code"));
		 int contentSeq = fileService.getFileListMaxContentSeq(String.valueOf(param.get("experId"))); 
		 
		 String oldFileName = "";
		 String surfix = "";
		 String newFileName = "";
	
		 String fileSize = "";
		 String fileType = "";
		 
		 // 저장될 폴더경로
		 String path = fileDir+"/"+ contentCode +"/" + year + "/" + month + "/";
		 
		 //디렉토리 없을 시, 디렉토리 생성  
		 File dir = new File(path);
		 if (!dir.isDirectory()) {
			 dir.mkdirs();
		 }
		 boolean result= false;
		 for(int i=0; i<fileList.length; i++) {
			 File file = new File(path + fileList[i].getOriginalFilename());
			 fileList[i].transferTo(file);
			 
			 oldFileName = fileList[i].getOriginalFilename();
			 surfix = oldFileName.substring(oldFileName.lastIndexOf("."),oldFileName.length());
			 newFileName = 	contentCode +"_"+
					 		new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.getDefault()).format(Calendar.getInstance().getTime()) +"_"+
					 		contentSeq+"_"+
					 		surfix; 
			 fileSize = String.valueOf(file.length());
			 fileType = new MimetypesFileTypeMap().getContentType(file);
			 
			 file.renameTo(new File(path+newFileName)); // 파일명 변경
			 
			 param.put("file_name", oldFileName);
			 param.put("file_size", fileSize);
			 param.put("file_type", fileType);
			 param.put("save_name", newFileName);
			 param.put("folder", path);
			 param.put("reg_id", "admin");
			 param.put("content_seq", param.get("exper_id"));
			 
			 
			 //DB저장
			 // 고유번호, 연결문서번호, 등록번호, 파일명, 사이즈, 타입,  저장된파일명,  저장된 폴더위치, 등록한사람, 등록날짜 , 사용여부
			/* for (Entry<String, Object> entry : param.entrySet()) {
	        		System.out.println( entry.getKey() + " / " + entry.getValue());
	        	}*/
			 
			 result = fileService.insertFileList(param);
			 
		 }
		 return result;
	 }
	 
	 @GetMapping("/file/3")
	 @ResponseBody
	 public boolean deleteExper(@RequestParam(required=false) String seq){
		 boolean result = false;
		 try{
			 result = fileService.deleteFile(seq);
		 }catch(Exception e){
			 logger.info(e);
         }
		 return result;
	 }
	 
}
