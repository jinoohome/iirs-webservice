package com.iirs.webservice.file.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;


@Mapper
public interface FileDao {
	
	@Select(
			" SELECT 	SEQ, CONTENT_CODE , CONTENT_SEQ, FILE_NAME, FILE_SIZE, \r\n" + 
			" 			TYPE,  SAVE_NAME, FOLDER, REG_ID, REG_DT, USE_YN \r\n" + 
			" FROM 		FILELIST\r\n" + 
			" WHERE 	CONTENT_SEQ  = #{exper_id} \r\n"+ 
			" AND 		USE_YN='Y' \r\n" 
			)
	public List<Map<String, Object>> getFileList(String experId);

	@Select(
			 " SELECT NVL(MAX(SEQ)+1,1) FROM FILELIST "
		)
	public int getFileListMaxSeq();

	@Select(
			 " SELECT NVL(MAX(SEQ),0) FROM FILELIST "
		)
	public int getFileListMaxContentSeq(String experId);

	@Insert(
		" INSERT INTO FILELIST "+
				" VALUES(	#{seq}, 		#{content_code}, 	#{content_seq}, 	\r\n"	+ 
				"			#{file_name}, 	#{file_size}, 		#{file_type}, 		\r\n" 	+ 
				" 	 		#{save_name}, 	#{folder}, 			#{reg_id},  		\r\n" 	+ 
				" 			sysdate,		'Y'										\r\n" 	+ 
				"	)"
		)
	public boolean insertFileList(Map<String, Object> param);
	
	@Update(
			"UPDATE FILELIST SET USE_YN = 'N' WHERE SEQ=#{seq}"
	)
	public boolean deleteFile(String seq);
}
