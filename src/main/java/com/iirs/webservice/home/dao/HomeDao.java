package com.iirs.webservice.home.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;


@Mapper
public interface HomeDao {
	
	@Insert(
				" INSERT INTO exper "+
				" VALUES(	\r\n" + 
				" 	'R'|| TO_CHAR(SYSDATE,'YYMMDDhh24miss'), \r\n" + 
				" 	#{experNm}, \r\n" + 
				"	SYSDATE \r\n" + 
				")"
				
	)public boolean insertExerInfo(Map<String, Object> param);

	
	@Select(
			"	<script>"+
			" 	SELECT EXPER_ID, EXPER_NM, TO_CHAR(REG_DT,'YY.MM.DD hh24:mi') REG_DT \r\n" + 
			" 	FROM EXPER \r\n"+
			"	<if test='value != null'>"+
			"	WHERE EXPER_ID || EXPER_NM || TO_CHAR(REG_DT,'YYYYMMDD') LIKE '%'||#{searchInput}||'%'"+
			"	</if>"+
			" 	order by 1 desc"+
			"	</script>"
			)
	public List<Map<String, Object>> getExperList(String searchInput);
	
	
	@Select(
			"	SELECT EXPER_ID, EXPER_NM, TO_CHAR(REG_DT,'YY.MM.DD hh24:mi') REG_DT \r\n" + 
			"	FROM EXPER \r\n" + 
			"	WHERE EXPER_ID = #{exper_id} "
			)
	public Map<String, Object> experDetail(String exper_id);


	@Delete(
			"	DELETE FROM EXPER \r\n" + 
			"	WHERE EXPER_ID = #{exper_id} "  
			)
	public boolean deleteExerInfo(String exper_id);


	@Update(
			"	UPDATE EXPER SET " +
			"	EXPER_NM =  #{experNm} " +
			"	WHERE EXPER_ID = #{experId} " 
			)
	
	public boolean updateExer(Map<String, Object> param);

	
}
