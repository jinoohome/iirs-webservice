package com.iirs.webservice.home.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iirs.webservice.exper.service.ExperService;
import com.iirs.webservice.file.service.FileService;

import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
public class HomeController {
	
	 private ExperService experService;
	 private FileService fileService;
	 private static final Logger logger = LogManager.getLogger();
	
	 @GetMapping("/home/1")
	    public String getExper(@RequestParam(required=false) String searchInput, Model model) {
		 List<Map<String,Object>> experList = new ArrayList<Map<String,Object>>();
		 try{
			 experList = experService.getExperList(searchInput);
    	 }catch(Exception e){
    		 logger.info(e);
         }
		 model.addAttribute("experList",experList);
		 return "home/home";
	 }
	 
	 @PostMapping("/home/2")
	 @ResponseBody
	 public boolean insertExper(@RequestParam Map<String,Object> param){
		boolean result = false;
		result = experService.insertExerInfo(param);
		return result;
	 }
	 
	 @GetMapping("/home/3")
	 @ResponseBody
	 public boolean deleteExper(@RequestParam(required=false) String exper_id){
		 boolean result = false;
		 try{
			 result = experService.deleteExerInfo(exper_id);
		 }catch(Exception e){
			 logger.info(e);
         }
		 return result;
	 }
	 
	 @PostMapping("/home/4")
	 @ResponseBody
	 public boolean updateExper(@RequestParam Map<String,Object> param){
		boolean result = false;
		try{
			result = experService.updateExer(param);
		}catch(Exception e){
			logger.info(e);
	     }
		return result;
	 }
	 
	 @GetMapping("/home/detail")
	 public String getExperDetail(@RequestParam(required=false) String exper_id, Model model) {
		 
		 Map<String,Object> experDetail = new HashMap<String,Object>();
		 List<Map<String, Object>> fileList = new ArrayList<Map<String, Object>>();
		 try{
			 experDetail = experService.experDetail(exper_id);
			 fileList = fileService.getFileList(exper_id);
		 }catch(Exception e){
			 logger.info(e);
	     }
		 
		 model.addAttribute("experDetail",experDetail);
		 model.addAttribute("fileList",fileList);
		 return "exper/experDetail";
	 }
	 
/*	 @PostMapping("/exper/6")
	 @ResponseBody
	 public boolean insertFile(@RequestParam("fileList") MultipartFile[] fileList) {
		
		 logger.info("테스트");
		 return true;
	 }*/

}
