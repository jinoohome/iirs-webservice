package com.iirs.webservice.exper.service;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;

import com.iirs.webservice.exper.dao.ExperDao;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ExperService {
	private ExperDao experDao;
	
	public boolean insertExerInfo(Map<String, Object> param) {
		return experDao.insertExerInfo(param);
	}

	public List<Map<String, Object>> getExperList(String searchInput) {
		List<Map<String, Object>> experList = experDao.getExperList(searchInput);
		return experList;
	}

	public Map<String, Object> experDetail(String exper_id) {
		Map<String, Object> experDetail = experDao.experDetail(exper_id);
		return experDetail;
	}

	public boolean deleteExerInfo(String exper_id) {
		return experDao.deleteExerInfo(exper_id);
	}

	public boolean updateExer(Map<String, Object> param) {
		return experDao.updateExer(param);
	}



}
