<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<title></title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
<!-- <script src="/js/dropzone.js"></script> -->

<style>
@font-face {
	font-family: 'GoyangIlsan';
	src:
		url('https://cdn.jsdelivr.net/gh/projectnoonnu/noonfonts_one@1.0/GoyangIlsan.woff')
		format('woff');
	font-weight: normal;
	font-style: normal;
}

body {
	font-family: 'GoyangIlsan', sans-serif;
	/*  background-color: #f5f5f5;  */
	/*  background-color: white;  */
}
</style>
<script>
function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Byte';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	};
function deleteFile(seq){
		if (confirm("정말 삭제하시겠습니까?") == false){
			return false;
		}
		
		$.ajax({
		       url: '/file/3',
		       method: 'GET',
		       dataType : 'json',
		       data: {seq, seq},
		       success: function(data) {
		     	  alert("선택하신 시험이 삭제 되었습니다");
		     	  location.reload();
		       }
		});

}

</script>

</head>
<body>
	<form id="form">
	<div class="wrapper">
		<nav class="navbar navbar-light bg-light">
	 		<div style="cursor:pointer;" onclick="location.href='/exper/1'">
				<div style="float:left">
					<a class="navbar-brand" href="#" align="right" style="margin: 0px;">
						<img src="/image/home.png" width="40" height="" alt=""> 
					</a>
				</div>
				<div align="center" style="float:left;padding-top:20px;">
					<div style="font-weight:bold;" >
						<div style="font-size:15pt;"></div>
						<!-- <div style="font-size:5pt;">International Institute of Reliability Assessment</div> -->
					</div>
				</div>
			</div> 
		</nav>
		<div id="header" style="padding:100px; 0px;">
			<div class="row">
				<div class="col-2">
				</div>
				<div class="col-lg-9 text-center">
					<div id="experId" style="font-size:1rem;">${experDetail.EXPER_ID}</div>
					<div id="experNm" style="font-size:2rem;">${experDetail.EXPER_NM}</div>
					
				</div>
				<div class="col-lg-1">
					<div style="margin-top:38px;">
						<button type="button" id="fakeUpdateBtn" class="btn btn-danger btn-block">수정</button>
						<button type="button" id="updateBtn" class="btn btn-danger btn-block" style="display:none">수정</button>
						<button type="button" id="cancelBtn" class="btn btn-secondary btn-block" style="display:none">취소</button>
					</div>
				</div>
			</div>
		</div>
		<div class="content">
			<div class="container_1" style="background-color: ;">
				<div class="container">
				
		  			<%-- <div class="row justify-content-md-center">
						<div class="col-12" style="font-size:12pt;">
							<div class="my-3 p-3 bg-white">
								<div class="mb-3">
									<div class="row">
										<div class="col-3">
											<label>${experDetail.EXPER_ID}_1</label>				
											<button type="button" id="aBtn" class="btn btn-outline-primary btn-block">시험의뢰서</button>
										</div>
										<div class="col-3">
											<label>${experDetail.EXPER_ID}_2</label>
											<button type="button" id="bBtn" class="btn  btn btn-outline-info btn-block">견적의뢰서</button>
										</div>
										<div class="col-3">
											<label>${experDetail.EXPER_ID}_3</label>
											<button type="button" id="cBtn" class="btn  btn-outline-secondary btn-block">기타보고서</button>
										</div>
										<div class="col-3">
											<label>${experDetail.EXPER_ID}_4</label>
											<button type="button" id="dBtn" class="btn btn-outline-dark btn-block">결과보고서</button>
										</div>
									</div>
								</div> 
							</div>
						</div>			
					</div> --%>
					<div align="">
						<div class="p-3 row" >
							<div class="col-10" >
							</div>
							<!-- <div class="col-3" >
								<select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
								<option selected>시험의뢰서</option>
								<option value="1">견적의뢰서</option>
								<option value="2">기타보고서</option>
								<option value="3">결과보고서</option>
								</select>
							</div> -->
							<div class="col-lg-2" >
								<button type="button" id="selectBtn" class="btn btn-primary btn-block">파일선택</button>
								<input multiple="multiple" type="file" name="files" id="files" style="display:none"/>
							</div>
						</div>
					</div>
						<div align="right">
							<div class="col-lg-8">
								<div class="border border-primary rounded" style="width:100%" >
									<div action="/upload-target" id="dropzone" class="dz-clickable">
										<div class="dz-default dz-message p-3" id="uploadList" style="overflow:auto;width:100%;height:100px;" >
											<span><i class="fas fa-plus-circle" style="color:green"></i> 업로드할 파일을 마우스로 끌어오세요</span>
										</div>
									</div>
										<!-- <input type="file" multiple="multiple" class="dz-hidden-input" style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;"> -->
								</div>
							</div>
						</div>
						<div align="right" class='pt-3'>
							<div class="col-lg-4">
								<button type="button" id="uploadBtn" class="btn btn-outline-primary btn-block"  style="display:none" >업로드 <i class="fas fa-cloud-upload-alt"></i></button>
							</div>
						</div>
						<br><br>
						<div>
							<table class="table table-hover">
								 <thead class="">
									<tr class="">
										<th style="padding-left:20px;" scope="col">파일명</th>
										<th scope="col">사이즈</th>
										<th scope="col"></th>
									</tr>
								</thead>
								<tbody>
								<c:choose>
								<c:when test="${not empty fileList}">
								<c:forEach var="fileList" items="${fileList}" varStatus="status">
									<tr>
										<td style="padding-left:20px;">${fileList.FILE_NAME}</td>
										<td><script>document.write(bytesToSize('${fileList.FILE_SIZE}'));</script></td>
										<td align="center">
										<a href="javascript:deleteFile('${fileList.SEQ}')">
										<button type="button" class="close" aria-label="Close">
									  		<span aria-hidden="true">&times;</span>
										</button>	
										</td>
									</tr>
								</c:forEach>
								</c:when>
								</c:choose>
								</tbody>
							</table>
						</div>
						<div align="center">
						</div>
					</div>
				</div>
			</div>
			
			<div class="container_2">
				<div class="container pt-5" align="right">
					<div id="boxA"> <strong></strong></div>
					<div id="big" ondragenter="return dragEnter(event)" ondrop="return dragDrop(event)" ondragover="return dragOver(event)"></div>
				</div>
			</div>
			<script>
			function dragStart(ev) {
				   ev.dataTransfer.effectAllowed='move';
				   ev.dataTransfer.setData("Text", ev.target.getAttribute('id'));   ev.dataTransfer.setDragImage(ev.target,100,100);
				   return true;
				}
			
			</script>
			
		</div>
		
		
		<div class="footer "style="margin-top:100px;padding:20px 0px 80px 30px;border-top:1px solid gainsboro;">
			<div class="container" >
				<div class="row">
					<div class="col-lg-9 pb-3">
						<div style="margin-top:10px;">
							<div style="float:left">
								<img src="/image/spring-boot.png" alt="" style="margin-top:2px;"> 
							</div>
							<div style="float:left">
								<img src="/image/mybatis.png" alt="" style="margin-top:2px;"> 
							</div>
							<div style="float:left">
								<img src="/image/gradle.png" alt="" style="margin-top:2px;"> 
							</div>
							<div align="center" style="float:left;padding-top:20px;">
								<div style="font-weight:bold;" >
									<div style="font-size:10pt;"></div>
								</div>
							</div>
						</div>
					</div> 
					
					<!-- <div class="col-6">
						<div style="text-align:left;margin: 20px 0px 0px 50px;">
							<span style="font-size:10pt;letter-spacing:1px;word-spacing:2px;">
								SpringBoot-myBatis 샘플 프로젝트 <br>
								H 010) 9497-6266  최진형<br><br>
							</span>
							<span style="font-size:8pt;letter-spacing:2px;">
								Copyright © JeanBro All rights reserved.
							</span>
						</div>
					</div>  -->
					
					<div class="col-lg-3 pt-3">
						<div style="float:left;margin-right:10px;" >
							<div style="font-size:11pt;">
								SpringBoot-myBatis 샘플 프로젝트 <br>
								H) 010-9497-6266 &nbsp;&nbsp; 최진형<br><br>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>	
				
	</div>
	<input type="hidden" name="experId" value="${experDetail.EXPER_ID}" >
	<input type="hidden" name="experNm" value="${experDetail.EXPER_NM}" >
    </form>
  <!--   <form name="form1" id="form1" action="" method="post" enctype="multipart/form-data">
	<div>
		<table id="uploadForm" style="display: block;"></table>
	</div>
	</form> -->

    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script  src="http://code.jquery.com/jquery-latest.min.js"></script>
    
</body>
<script>
var fileList= new Array();
var uploadList="";
var sizeType="";

$(function() {
	var obj = $("#dropzone");
    obj.on('dragenter', function (e) {
         e.stopPropagation();
         e.preventDefault();

    });

    obj.on('dragleave', function (e) {
         e.stopPropagation();
         $(this).css('background', '');
         e.preventDefault();
    });

    obj.on('dragover', function (e) {
         e.stopPropagation();
         e.preventDefault();
         $(this).css('background-image', 'linear-gradient(-45deg, rgba(115, 33, 33, 0) 0%, rgba(95, 20, 20, 0) 44%, rgba(255, 260, 255, 0.3) 45%, rgba(33, 150, 243, 0.28) 55%, rgba(255, 255, 255, 0) 56%, rgba(255, 255, 255, 0) 100%)');
         $(this).css('background-size', '10px 10px');
         $(this).css('background-repeat', 'repeat');
    });
    

    obj.on('drop', function (e) {
         e.preventDefault();
         var files = e.originalEvent.dataTransfer.files;
         if(files.length < 1){
              return;
         }
        
         $(this).css('background', '');
                 
         addUploadList(files);
    });
    
	/*선택 버튼 클릭  */
	$('#selectBtn').bind('click', function() {
        $('#files').trigger('click');
    });
	
	/* 실제 input file 변경시 */
	 $('#files').bind('change', function() {
         
		 addUploadList(this.files);
	 });
    
    function addUploadList(files){
		for (var i = 0; i < files.length; i++) {
			fileListNum = fileList.length;
			fileList[fileListNum] = files[i];
		}

        reloadUploadList(fileList);
         //F_FileMultiUpload(files, obj);
    }
    
    /*업로드 리스트 파일 취소 */
    $(document).on('click','.cancelFile',function(e) {
		var fileCancleBtnId = $(this).attr("id");
		var index = fileCancleBtnId.replace(/[^0-9]/g,"");
		fileList.splice(index,1);
		$(this).parent().parent().remove(".row");
		reloadUploadList(fileList);
	});
    
    /* 업로드리스트 재구성하기 */
    function reloadUploadList(fileList){
    	$('#uploadList').empty();		
		uploadList = "";
		if(fileList.length == 0){
			uploadList = '<span><i class="fas fa-plus-circle" style="color:green"></i> 업로드할 파일을 마우스로 끌어오세요</span>';
			$('#uploadList').html(uploadList);
			
			$('#uploadBtn').css('display','none');
		}else{
			for (var i = 0; i < fileList.length; i++) {
				sizeType = bytesToSize(fileList[i].size);
			    uploadList += 	"<div class='row'>"+
									"<div class='col-8' style='text-align:left;'>"+fileList[i].name.substring(0,30)+"</div>"+
			      					"<div class='col-3'>"+sizeType+"</div>"+
			      					"<div class='col-1'><a href='#' id='uploadCancelBtn"+i+"' class='cancelFile' style='cursor:pointer'>"+"X"+"</a></div>"+
		      					"</div>";
		    }
	       	$('#uploadBtn').css('display','');
		}
         $('#uploadList').html(uploadList);
    }
    
    $('#uploadBtn').click(function() {
    	var con_test = confirm("리스트에 있는 파일을 업로드를 하실겁니까? ");
    	if(!con_test){
  			return false;
    	}
    	
    	var data = new FormData();
        for (var i = 0; i < fileList.length; i++) {
	       data.append('fileList',fileList[i]);
         }
        data.append('content_code','exper');
        data.append('exper_id','${experDetail.EXPER_ID}');
        $.ajax({
           url: '/file/2',
           enctype: 'multipart/form-data',
           method: 'POST',
           data: data,
           dataType: 'json',
           processData: false,
           contentType: false,
           success: function(res) {
        	   location.reload();
           }
        });
	});
    
    

    // 파일 멀티 업로드
    function F_FileMultiUpload(files) {
         
    }

    // 파일 멀티 업로드 Callback
    function F_FileMultiUpload_Callback(files) {
    }
    
	
	$('#fakeUpdateBtn').bind("click",function() {
		var experNmVal = $('#experNm').html();
		$('#experNm').html("<input type='text' name='experNm' value='"+experNmVal+"' class='form-control' style='text-align:center;font-size:28pt;height:60px;'>");
		$('#cancelBtn').css("display","");
		$('#fakeUpdateBtn').css("display","none");
		$('#updateBtn').css("display","");
		
	});

	$('#updateBtn').bind("click",function() {
		$('#fakeUpdateBtn').css("display","");
		$('#updateBtn').css("display","none");
		$('#cancelBtn').css("display","none");
		
		 $.ajax({
		        type: 'POST',
		        url: '/exper/4',
		        dataType : 'json',
		        data: $("#form").serialize(), //보낼데이터
		        success: function(data) {
		        	location.reload();
		        },
		    });  
		
	});
	
	$('#cancelBtn').bind("click",function() {
		$('#experNm').html("${experDetail.EXPER_NM}");
		$('#fakeUpdateBtn').css("display","");
		$('#updateBtn').css("display","none");
		$('#cancelBtn').css("display","none");
		
	});
	
	$('#expEnrBtn').bind("click",function() {
		 $.ajax({
		        type: 'POST',
		        url: '/exper/2',
		        dataType : 'json',
		        data: $("#form").serialize(), //보낼데이터
		        success: function(data) {
		        	alert("입력하신 시험이 성공적으로 등록 되었습니다");
		        },
		    });  
		
	});
	
	/* $('#aBtn').click(function () {
		$('body,html').animate({scrollTop: 500}, 500);
	    return false;
	});
	$('#bBtn').click(function () {
		$('body,html').animate({scrollTop: 500}, 500);
	    return false;
	});
	$('#cBtn').click(function () {
		$('body,html').animate({scrollTop: 500}, 500);
	    return false;
	});
	$('#dBtn').click(function () {
		$('body,html').animate({scrollTop: 500}, 500);
	    return false;
	}); */
	
	
});


</script>
</html>

