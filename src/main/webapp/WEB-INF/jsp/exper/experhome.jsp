<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<title></title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
<style>
@font-face {
	font-family: 'GoyangIlsan';
	src:
		url('https://cdn.jsdelivr.net/gh/projectnoonnu/noonfonts_one@1.0/GoyangIlsan.woff')
		format('woff');
	font-weight: normal;
	font-style: normal;
}

body {
	font-family: 'GoyangIlsan', sans-serif;
	/*  background-color: #f5f5f5;  */
	/*  background-color: white;  */
}

.back-to-top {
    cursor: pointer;
    position: fixed;
    bottom: 20px;
    right: 20px;
    display:none;
}

</style>
</head>
<body>
	<form id="form" name="form">
	<div class="wrapper">
		<nav class="navbar navbar-light bg-light">
	 		<div style="cursor:pointer;" onclick="location.href='/exper/1'">
				<div style="float:left">
					<a class="navbar-brand" href="#" align="right" style="margin: 0px;">
						<img src="/image/home.png" width="40" height="" alt=""> 
					</a>
				</div>
				<div align="center" style="float:left;padding-top:20px;">
					<div style="font-weight:bold;" >
						<div style="font-size:15pt;"></div>
						<!-- <div style="font-size:5pt;">International Institute of Reliability Assessment</div> -->
					</div>
				</div>
			</div> 
		</nav>
		<div id="header" style="">
			
			<div class="container_2" style="background-color: ;">
				<div class="container" align="center">
				
		  			<div class="row justify-content-md-center">
						<div class="col-12" style="font-size:13pt;">
							<div class="my-3 p-3 bg-white">
								<div class="mb-3">
								<div class="col-md mb-5 mt-3">
									<h2>Spring-myBatis<span class="d-none d-sm-block">샘플 프로젝트</span></h2>
								</div>
								<div class="row">
									<div class="col-1">
									</div>
									<div class="col-lg-9 pb-3">
										<input type="text" class="form-control" id="experNm"  name="experNm" style=""
										onkeypress="if(event.keyCode==13)enrollEnter();">
									</div>
									<div class="col-lg-2">
										<button type="button" class="btn btn-primary btn-block" id="expEnrBtn" name="expEnrBtn">등록</button>
									</div>
								</div>
								</div>
							</div>
						</div>			
					</div>
				
				</div>
			</div>
		</div>
		<div class="content">
			<div class="container_1  pt-5" style="" >
				<div class="container" align="right">
				
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-8"></div>
							<div class="col-md-3 pb-3">
								<input type="text" class="form-control" id="searchInput" name="searchInput" placeholder="Search" 
								onkeypress="if(event.keyCode==13)searchEnter();" >
							</div>
							<div class="col-md-1 text-center">
								<button class="btn btn-outline-primary btn-block" id="searchBtn" >검색</button>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="container_3">
				<div class="container pt-2" align="center">
				
					<table class="table text-center table table-striped">
						<tr>
							<th style="width:10%"></th>	
							<th class="d-none d-sm-block">등록코드</th>	
							<th style="width:40%">제목</th>	
							<th class="d-none d-sm-block">등록일</th>	
							<th style="width:20%"></th>	
						</tr>
						<c:choose>
						<c:when test="${not empty experList}">
						<c:forEach var="exper" items="${experList}" varStatus="status">
						<tr>
							<td>${status.count}</td>	
							<td class="d-none d-sm-block">${exper.EXPER_ID}</td>	
							<td><a href="javascript:sendExperDtail('${exper.EXPER_ID}')">${exper.EXPER_NM}</a></td>	
							<td class="d-none d-sm-block">${exper.REG_DT}</td>	
							<td><a href="javascript:deleteExper('${exper.EXPER_ID}')">
								<button type="button" class="close" aria-label="Close">
							  		<span aria-hidden="true">&times;</span>
								</button>								
								</a>
							</td>	
						</tr>
						</c:forEach>
						</c:when>
						</c:choose>
				
					</table>
					
				</div>
			</div>
			<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" data-toggle="tooltip" >
				<i class="fas fa-chevron-up"></i>
			</a>
	   
			
		</div>
		<div class="footer "style="margin-top:100px;padding:20px 0px 80px 30px;border-top:1px solid gainsboro;">
			<div class="container" >
			<div class="row">
				<div class="col-lg-9 pb-3">
					<div style="margin-top:10px;">
						<div style="float:left">
							<img src="/image/spring-boot.png" alt="" style="margin-top:2px;"> 
						</div>
						<div style="float:left">
							<img src="/image/mybatis.png" alt="" style="margin-top:2px;"> 
						</div>
						<div style="float:left">
							<img src="/image/gradle.png" alt="" style="margin-top:2px;"> 
						</div>
						<div align="center" style="float:left;padding-top:20px;">
							<div style="font-weight:bold;" >
								<div style="font-size:10pt;"></div>
							</div>
						</div>
					</div>
				</div> 
				
				<!-- <div class="col-6">
					<div style="text-align:left;margin: 20px 0px 0px 50px;">
						<span style="font-size:10pt;letter-spacing:1px;word-spacing:2px;">
							SpringBoot-myBatis 샘플 프로젝트 <br>
							H 010) 9497-6266  최진형<br><br>
						</span>
						<span style="font-size:8pt;letter-spacing:2px;">
							Copyright © JeanBro All rights reserved.
						</span>
					</div>
				</div>  -->
				
				<div class="col-lg-3 pt-3">
					<div style="float:left;margin-right:10px;" >
						<div style="font-size:11pt;">
							SpringBoot-myBatis 샘플 프로젝트 <br>
							H) 010-9497-6266 &nbsp;&nbsp; 최진형<br><br>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>	
	</div>
    </form>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script  src="http://code.jquery.com/jquery-latest.min.js"></script>
</body>
<script>
$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $('#back-to-top').fadeIn();
    } else {
        $('#back-to-top').fadeOut();
    }
});
$('#back-to-top').click(function () {
	  $('body,html').animate({scrollTop: 0}, 300);
    return false;
});

var sendExperDtail = function(exper_id){
	var url = "/exper/detail?exper_id="+exper_id;
	location.href= url;
}

var searchEnter = function(){
	$('#searchBtn').trigger("click");
}

var enrollEnter = function(){
	$('#expEnrBtn').trigger("click");
} 

var deleteExper = function(exper_id){
	
	if (confirm("정말 삭제하시겠습니까??") == true)   document.form.submit();
	else return false;

	 $.ajax({
	        type: 'GET',
	        url: '/exper/3',
	        dataType : 'json',
	        data: {exper_id, exper_id}, //보낼데이터
	        success: function(data) {
	        	alert("선택하신 시험이 삭제 되었습니다");
	        },
	    });  
	/*  location.reload(); */
} 

$(function() {
	$('#expEnrBtn').bind("click",function() {
		if(!$('#experNm').val()){
			alert("입력창에 내용을 입력해주세요");
			return false;
		}else{
			
			$.ajax({
				type: 'POST',
				url: '/exper/2',
				dataType : 'json',
				data: $("#form").serialize(), //보낼데이터
				success: function(data) {
					alert("입력하신 시험이 성공적으로 등록 되었습니다");
					location.reload();
				},
			}); 
			
		}
	});
	
	$('#searchBtn').bind("click",function() {
		 $.ajax({
		        type: 'GET',
		        url: '/exper/1',
		        dataType : 'json',
		        data: $("#form").serialize(), //보낼데이터
		        success: function(data) {
		        },
		    });  
	});
	
	
});

</script>

</html>




