<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<title></title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
<style>
@font-face {
	font-family: 'GoyangIlsan';
	src:
		url('https://cdn.jsdelivr.net/gh/projectnoonnu/noonfonts_one@1.0/GoyangIlsan.woff')
		format('woff');
	font-weight: normal;
	font-style: normal;
}

body {
	font-family: 'GoyangIlsan', sans-serif;
	/*  background-color: #f5f5f5;  */
	/*  background-color: white;  */
}

.back-to-top {
    cursor: pointer;
    position: fixed;
    bottom: 20px;
    right: 20px;
    display:none;
}

</style>
</head>
<body>
	<form id="form" name="form">
	<div class="wrapper">
		<nav class="navbar navbar-light bg-light">
	 		<div style="cursor:pointer;" onclick="location.href='/exper/1'">
				<div style="float:left">
					<a class="navbar-brand" href="#" align="right" style="margin: 0px;">
						<img src="/image/iira_logo.png" width="70" height="70" alt=""> 
					</a>
				</div>
				<div align="center" style="float:left;padding-top:20px;">
					<div style="font-weight:bold;" >
						<div style="font-size:15pt;">국제신뢰성평가센터</div>
						<div style="font-size:5pt;">International Institute of Reliability Assessment</div>
					</div>
				</div>
			</div> 
		</nav>
		<div id="header" style="padding-top:100px;">
			
		</div>
		<div class="content" >
			<div class="container" style="height:220px;">
				<div class="container" align="center">
					<div style="font-size:70pt;color:darkorange;">404</div>
					<div style="font-size:35pt;color:gray;">요청하신 페이지는 없는 페이지입니다</div>
					<br><br><br>
					<div style="font-size:24pt;color:#007bff;cursor:pointer;" onclick="location.href='/exper/1'">< &nbsp;첫 페이지로 이동</div>
					
				</div>
			</div>
		</div>
		<div class="footer "style="margin-top:250px;padding:20px 0px 80px 30px;border-top:1px solid gainsboro;">
			<div class="container" >
			<div class="row">
				<div class="col-3">
					<div style="margin-top:10px;">
						<div style="float:left">
							<img src="/image/iira_logo.png" width="70" height="70" alt="" style="margin-top:2px;"> 
						</div>
						<div align="center" style="float:left;padding-top:20px;">
							<div style="font-weight:bold;" >
								<div style="font-size:15pt;">국제신뢰성평가센터</div>
								<div style="font-size:5pt;">International Institute of Reliability Assessment</div>
							</div>
						</div>
					</div>
				</div> 
				
				<div class="col-6">
					<div style="text-align:left;margin: 20px 0px 0px 50px;">
						<span style="font-size:10pt;letter-spacing:1px;word-spacing:2px;">
							국제신뢰성평가센터  사업자등록번호: 116-81-142345  대표자 정현석<br>
							충남 아산시 신창면 순천향로 22번지 순천향대학교 앙뜨레프레너관 E204호<br>  
							T. 041)428-9490   F. 041)428-9493<br><br>
						</span>
						<span style="font-size:8pt;letter-spacing:2px;">
							Copyright © IIRA All rights reserved.
						</span>
					</div>
				</div> 
				
				<div class="col-3">
					<div style="margin:20px 0px 0px 30px;">
						<div style="float:left;margin-right:10px;" >
							<img src="/image/koras.png" width="70" height="" alt=""> 
							<div style="font-size:9pt;">국제공인시험기간</div>
						</div>
						<div style="float:left;text-align:center">
							<img src="/image/kibo.png" width="90" height="" alt="" style="margin-top:-5px;">
							<div style="font-size:9pt;margin-top:4px;">기술보증기금</div> 
						</div>
					</div> 
				</div>
			</div>
		</div>
		</div>	
	</div>
    </form>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script  src="http://code.jquery.com/jquery-latest.min.js"></script>
</body>
<script>

</script>

</html>




