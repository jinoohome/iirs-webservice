<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<title></title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
<link rel="stylesheet" href="path/to/css/feathericon.min.css">

<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<style>
@font-face {
	font-family: 'GoyangIlsan';
	src:
		url('https://cdn.jsdelivr.net/gh/projectnoonnu/noonfonts_one@1.0/GoyangIlsan.woff')
		format('woff');
	font-weight: normal;
	font-style: normal;
}

body {
	font-family: 'GoyangIlsan', sans-serif;
	/*  background-color: #f5f5f5;  */
	/*  background-color: white;  */
}

.back-to-top {
    cursor: pointer;
    position: fixed;
    bottom: 20px;
    right: 20px;
    display:none;
}
 .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

@media (min-width: 768px) {
  .bd-placeholder-img-lg {
    font-size: 3.5rem;
  }
}
a:link{ text-decoration: none;}
</style>
</head>

<body>
	<form id="form" name="form">
	<div class="wrapper">

	    <nav class="navbar navbar-dark bg-dark flex-md-nowrap p-0 m-0">
		       	<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Company name</a>
		    	<ul class="navbar-nav mr-auto mt-2 mt-lg-0 ml-3 ">
	      			<li class="nav-item active">
	           			 <a class="fa fa-bars text-white" href=""></a>
				      </li>
			    </ul>
		</nav>

		<div class="container-fluid">
		  <div class="row">
		    <nav class="col-md-2 d-none d-md-block bg-dark sidebar pr-0">
		    	<div>
		    		CHOI JIN HYUNG<br>
		    		Profile
		    	</div>
				<div class="sidebar-sticky" style="background:#D7D6DB;border-radius: 10px 0px 0px 10px; width:100%;">
				  <ul class="nav flex-column" >
				    <li class="nav-item pl-2 py-3">
				    	<i class="fe fe-heart">tset</i>
				    </li>
				    <li class="nav-item pl-2 py-2">
				    	 <a class="fas fa-home text-muted" href="">&nbsp;&nbsp;MENU1</a>
				    </li>
				    <li class="nav-item pl-2 py-2">
				    	 <a class="fas fa-home text-muted" href="">&nbsp;&nbsp;MENU2</a>
				    </li>
				    <li class="nav-item pl-2 py-2">
				    	 <a class="fas fa-home text-muted" href="">&nbsp;&nbsp;MENU2</a>
				    </li>
				    <li class="nav-item pl-2 py-2">
				    	 <a class="fas fa-home text-muted" href="">&nbsp;&nbsp;MENU2</a>
				    </li>
				  </ul>
				</div>
		    </nav>
		
		    <main role="main" class="shadow col-md-9 ml-sm-auto col-lg-10 px-4 ">
		      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
		        <h1 class="h2">한글은</h1>
		        <div class="btn-toolbar mb-2 mb-md-0">
		          <div class="btn-group mr-2">
		            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
		            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
		          </div>
		          <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
		            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
		            This week
		          </button>
		        </div>
		      </div>
		
		
		      <h2>Section title</h2>
		      <div class="table-responsive">
		        <table class="table table-striped table-sm">
		          <thead>
		            <tr>
		              <th>#</th>
		              <th>Header</th>
		              <th>Header</th>
		              <th>Header</th>
		              <th>Header</th>
		            </tr>
		          </thead>
		          <tbody>
		            <tr>
		              <td>1,001</td>
		              <td>Lorem</td>
		              <td>ipsum</td>
		              <td>dolor</td>
		              <td>sit</td>
		            </tr>
		            <tr>
		              <td>1,002</td>
		              <td>amet</td>
		              <td>consectetur</td>
		              <td>adipiscing</td>
		              <td>elit</td>
		            </tr>
		            <tr>
		              <td>1,003</td>
		              <td>Integer</td>
		              <td>nec</td>
		              <td>odio</td>
		              <td>Praesent</td>
		            </tr>
		            <tr>
		              <td>1,003</td>
		              <td>libero</td>
		              <td>Sed</td>
		              <td>cursus</td>
		              <td>ante</td>
		            </tr>
		          </tbody>
		        </table>
		      </div>
		    </main>
		  </div>
		</div>
	</div>
	</form>
  	
  	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script  src="http://code.jquery.com/jquery-latest.min.js"></script>

</body>


</html>




